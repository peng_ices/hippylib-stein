# Copyright (c) 2016-2018, The University of Texas at Austin
# & University of California, Merced.
#
# All Rights reserved.
# See file COPYRIGHT for details.
#
# This file is part of the hIPPYlib library. For more information and source code
# availability see https://hippylib.github.io.
#
# hIPPYlib is free software; you can redistribute it and/or modify it under the
# terms of the GNU General Public License (as published by the Free
# Software Foundation) version 2.0 dated June 1991.

from __future__ import absolute_import, division, print_function

import dolfin as dl
import numpy as np

import sys
import os

sys.path.append(os.environ.get('HIPPYLIB_BASE_DIR', "../../"))
from hippylib import *


class FluxQOI(object):
    def __init__(self, Vh, dsGamma):
        self.Vh = Vh
        self.dsGamma = dsGamma
        self.n = dl.Constant((0., 1.))  # dl.FacetNormal(Vh[STATE].mesh())

        self.u = None
        self.m = None
        self.L = {}

    def form(self, x):
        # return dl.avg(dl.exp(x[PARAMETER])*dl.dot( dl.grad(x[STATE]), self.n) )*self.dsGamma
        return dl.exp(x[PARAMETER]) * dl.dot(dl.grad(x[STATE]), self.n) * self.dsGamma

    def eval(self, x):
        """
        Given x evaluate the cost functional.
        Only the state u and (possibly) the parameter a are accessed.
        """
        u = vector2Function(x[STATE], self.Vh[STATE])
        m = vector2Function(x[PARAMETER], self.Vh[PARAMETER])
        return dl.assemble(self.form([u, m]))


class GammaBottom(dl.SubDomain):
    def inside(self, x, on_boundary):
        return (abs(x[1]) < dl.DOLFIN_EPS)


def u_boundary(x, on_boundary):
    return on_boundary and (x[1] < dl.DOLFIN_EPS or x[1] > 1.0 - dl.DOLFIN_EPS)


def v_boundary(x, on_boundary):
    return on_boundary and (x[0] < dl.DOLFIN_EPS or x[0] > 1.0 - dl.DOLFIN_EPS)


dl.set_log_active(False)
sep = "\n" + "#" * 80 + "\n"
ndim = 10
nx = 8
ny = 8
num_terms = 2
mesh = dl.UnitSquareMesh(nx, ny)

rank = dl.MPI.rank(mesh.mpi_comm())
nproc = dl.MPI.size(mesh.mpi_comm())

Vh_STATE = dl.FunctionSpace(mesh, 'Lagrange', 1)
Vh_PARAMETER = dl.VectorFunctionSpace(mesh, "R", degree=0, dim=num_terms)
Vh = [Vh_STATE, Vh_PARAMETER, Vh_STATE]

ndofs = [Vh[STATE].dim(), Vh[PARAMETER].dim(), Vh[ADJOINT].dim()]
if rank == 0:
    print(sep, "Set up the mesh and finite element spaces", sep)
    print("Number of dofs: STATE={0}, PARAMETER={1}, ADJOINT={2}".format(*ndofs))

# Initialize Expressions
f = dl.Constant(0.0)

u_bdr = dl.Expression("x[1]", degree=1)
u_bdr0 = dl.Constant(0.0)
bc = dl.DirichletBC(Vh[STATE], u_bdr, u_boundary)
bc0 = dl.DirichletBC(Vh[STATE], u_bdr0, u_boundary)

k_block_1d = np.int(np.sqrt(num_terms))
x1 = np.linspace(0., 1., k_block_1d + 1)
x2 = np.linspace(0., 1., k_block_1d + 1)
step = 1. / k_block_1d

indicatorlist = []
for n in range(num_terms):
    j = np.mod(n, k_block_1d)
    i = (n - j) / k_block_1d
    indicatorlist.append(dl.Expression("sin((i+1)*pi*x[0])*sin((j+1)*pi*x[1])", i=i, j=j, degree=1))  # /pow((n+1),0.5)
indicatorlist = dl.as_vector(indicatorlist)


def pde_varf(u, m, p):

    varf = dl.exp(dl.inner(indicatorlist, m)) \
          * dl.inner(dl.nabla_grad(u), dl.nabla_grad(p)) * dl.dx - f * p * dl.dx  # + self.g*p*dl.ds

    return varf

pde = PDEVariationalProblem(Vh, pde_varf, bc, bc0, is_fwd_linear=True)
if dlversion() <= (1, 6, 0):
    pde.solver = dl.PETScKrylovSolver("cg", amg_method())
    pde.solver_fwd_inc = dl.PETScKrylovSolver("cg", amg_method())
    pde.solver_adj_inc = dl.PETScKrylovSolver("cg", amg_method())
else:
    pde.solver = dl.PETScKrylovSolver(mesh.mpi_comm(), "cg", amg_method())
    pde.solver_fwd_inc = dl.PETScKrylovSolver(mesh.mpi_comm(), "cg", amg_method())
    pde.solver_adj_inc = dl.PETScKrylovSolver(mesh.mpi_comm(), "cg", amg_method())
pde.solver.parameters["relative_tolerance"] = 1e-15
pde.solver.parameters["absolute_tolerance"] = 1e-20
pde.solver_fwd_inc.parameters = pde.solver.parameters
pde.solver_adj_inc.parameters = pde.solver.parameters

ntargets = 30
np.random.seed(seed=1)
targets = np.random.uniform(0.1, 0.9, [ntargets, ndim])
if rank == 0:
    print("Number of observation points: {0}".format(ntargets))
misfit = PointwiseStateObservation(Vh[STATE], targets)

prior = FiniteDimensionalPrior(Vh_PARAMETER, mesh)

# Generate synthetic observations
noise = dl.Vector()
prior.init_vector(noise, "noise")
parRandom.normal(1., noise)
mtrue = dl.Vector()
prior.init_vector(mtrue, 0)
prior.sample(noise, mtrue)

filename = 'data/particle_true'
np.savez(filename, particle_true=mtrue.get_local())

utrue = pde.generate_state()
x = [utrue, mtrue, None]
pde.solveFwd(x[STATE], x, 1e-9)
filename = 'data/state_true.xdmf'
state_fun = dl.Function(Vh[STATE], name='state')
state_fun.vector().axpy(1.0, x[STATE])
if dlversion() <= (1, 6, 0):
    dl.File(mesh.mpi_comm(), filename) << state_fun
else:
    xf = dl.XDMFFile(mesh.mpi_comm(), filename)
    xf.write(state_fun)

misfit.B.mult(x[STATE], misfit.d)
rel_noise = 0.1
MAX = misfit.d.norm("linf")
noise_std_dev = rel_noise * MAX
parRandom.normal_perturb(noise_std_dev, misfit.d)
misfit.noise_variance = noise_std_dev * noise_std_dev

model = Model(pde, prior, misfit)


