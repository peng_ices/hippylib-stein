from model_karhunenloeve import *

import time

# check the stein/options to see all possible choices
options["type_parameter"] = 'vector'
options["number_particles"] = 2
options["number_particles_add"] = 0
options["add_number"] = 0
options["add_step"] = 10
options["add_rule"] = 2
options["type_Hessian"] = "lumped"
options["max_iter"] = 40
options["gauss_newton_approx"] = True  # if error of unable to solve linear system occurs, use True
options["max_iter_delta_kernel"] = 0
options["type_metric"] = "posterior_average"
options["low_rank_Hessian_misfit"] = 1

# a class to generate and add particles
particle = Particle(model, options)

# a class to evaluate the variation (gradient, Hessian) of the negative log likelihood function at given particles
variation = Variation(model, particle, options)

# a class to evaluate the kernel and its gradient at given particles
kernel = Kernel(model, particle, variation, options)

t0 = time.time()

solver = NewtonCoupled(model, particle, variation, kernel, options)

solver.solve()

print("NewtonCoupled solving time = ", time.time() - t0)
