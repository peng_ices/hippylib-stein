from model_karhunenloeve import *

import time

# check the stein/options to see all possible choices
options["type_parameter"] = 'vector'
options["rank_Hessian_misfit"] = prior.dim
options["number_particles"] = 20
options["number_particles_add"] = 0
options["add_number"] = 0
options["add_step"] = 5
options["add_rule"] = 1
options["type_Hessian"] = "lumped"
options["max_iter"] = 10
options["gauss_newton_approx"] = False  # if error of unable to solve linear system occurs, use True
options["max_iter_delta_kernel"] = 0
options["type_metric"] = "posterior_average"
options["low_rank_Hessian_misfit"] = 1
options["low_rank_Hessian_average"] = False
options["delta_kernel"] = False
options["line_search"] = False
options["max_backtracking_iter"] = 3

# generate particles
particle = Particle(model, options)

# evaluate the variation (gradient, Hessian) of the negative log likelihood function at given particles
variation = Variation(model, particle, options)

# evaluate the kernel and its gradient at given particles
kernel = Kernel(model, particle, variation, options)

t0 = time.time()

solver = NewtonSeparated(model, particle, variation, kernel, options)

solver.solve()

print("NewtonSeparated solving time = ", time.time() - t0)
