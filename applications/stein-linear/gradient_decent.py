from model_linear import *

import time

# check the stein/options to see all possible choices
options["number_particles"] = 10
options["number_particles_add"] = 0
options["add_number"] = 0
options["add_step"] = 10
options["add_rule"] = 1
options["max_iter"] = 50
options["type_Hessian"] = "lumped"
options["search_size"] = 1.e-2
options["line_search"] = True
options["max_backtracking_iter"] = 10

# generate particles
particle = Particle(model, options)

# evaluate the variation (gradient, Hessian) of the negative log likelihood function at given particles
variation = Variation(model, particle, options)

# evaluate the kernel and its gradient at given particles
kernel = Kernel(model, particle, variation, options)

t0 = time.time()

solver = GradientDecent(model, particle, variation, kernel, options)

solver.solve()

print("GradientDecent solving time = ", time.time() - t0)
