from model_linear import *

import time

# check the stein/options to see all possible choices
options["number_particles"] = 1
options["number_particles_add"] = 0
options["add_number"] = 0
options["add_step"] = 5
options["add_rule"] = 1
options["type_Hessian"] = "full"
options["type_scaling"] = 3
options["max_iter"] = 10
options["gauss_newton_approx"] = False  # if error of unable to solve linear system occurs, use True
options["max_iter_delta_kernel"] = 0
options["type_metric"] = "posterior_average"
options["low_rank_Hessian_misfit"] = 2
options["low_rank_Hessian_average"] = False
options["line_search"] = True
options["max_backtracking_iter"] = 10
options["cg_coarse_tolerance"] = 0.5e-2

# a class to generate and add particles
particle = Particle(model, options)

# a class to evaluate the variation (gradient, Hessian) of the negative log likelihood function at given particles
variation = Variation(model, particle, options)

# a class to evaluate the kernel and its gradient at given particles
kernel = Kernel(model, particle, variation, options)

t0 = time.time()

solver = NewtonCoupled(model, particle, variation, kernel, options)

solver.solve()

print("NewtonCoupled solving time = ", time.time() - t0)


print("samples from Laplace approximation")
for n in range(particle.number_particles):
    sampler = GaussianLRPosterior(model.prior, variation.d[n], variation.U[n], mean=particle.particles[n])
    noise = dl.Vector()
    model.prior.init_vector(noise, "noise")
    for m in range(10):
        s_prior = model.generate_vector(PARAMETER)
        s_posterior = model.generate_vector(PARAMETER)
        parRandom.normal(1., noise)
        model.prior.sample(noise, s_prior, add_mean=False)
        sampler.sample(s_prior, s_posterior, add_mean=True)
        x_star = model.generate_vector()
        # update the parameter
        x_star[PARAMETER].zero()
        x_star[PARAMETER].axpy(1., s_posterior)
        # update the state at new parameter
        x_star[STATE].zero()
        model.solveFwd(x_star[STATE], x_star)

        # evaluate the cost functional, here the potential
        cost, reg, misfit = model.cost(x_star)
        print("posterior cost, reg, misfit = ", cost, reg, misfit)

        s_prior = model.generate_vector(PARAMETER)
        model.prior.sample(noise, s_prior, add_mean=True)
        x_star[PARAMETER].zero()
        x_star[PARAMETER].axpy(1., s_prior)
        # update the state at new parameter
        x_star[STATE].zero()
        model.solveFwd(x_star[STATE], x_star)

        # evaluate the cost functional, here the potential
        cost, reg, misfit = model.cost(x_star)
        print("prior cost, reg, misfit = ", cost, reg, misfit)

