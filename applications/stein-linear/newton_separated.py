from model_linear import *

import time

# check the stein/options to see all possible choices
options["number_particles"] = 2
options["number_particles_add"] = 0
options["add_number"] = 1
options["add_step"] = 5
options["add_rule"] = 1
options["delta_kernel"] = False
options["type_Hessian"] = "lumped"
options["type_scaling"] = 3
options["max_iter"] = 100
options["gauss_newton_approx"] = False  # if error of unable to solve linear system occurs, use True
options["max_iter_delta_kernel"] = 0
options["type_metric"] = "posterior_separate"
options["low_rank_Hessian_misfit"] = 2
options["low_rank_Hessian_average"] = False
options["line_search"] = True
options["max_backtracking_iter"] = 10
options["cg_coarse_tolerance"] = 0.5e-2


# generate particles
particle = Particle(model, options)

# evaluate the variation (gradient, Hessian) of the negative log likelihood function at given particles
variation = Variation(model, particle, options)

# evaluate the kernel and its gradient at given particles
kernel = Kernel(model, particle, variation, options)

t0 = time.time()

solver = NewtonSeparated(model, particle, variation, kernel, options)

solver.solve()

print("NewtonSeparated solving time = ", time.time() - t0)


mean, mean_norm = particle.mean()
variance, variance_norm = particle.pointwise_variance()
print("mean", mean_norm, "variance", variance_norm)

posterior = GaussianLRPosterior(model.prior, variation.d[0], variation.U[0], mean=particle.particles[0])
post_pointwise_variance, _, _ = posterior.pointwise_variance(method="Randomized", r=100)

phelp = model.generate_vector(PARAMETER)
model.prior.M.mult(post_pointwise_variance, phelp)

print("variance", np.sqrt(post_pointwise_variance.inner(phelp)))


if particle.number_particles == 1:
    N = 100
    print("samples from Laplace approximation")
    for n in range(particle.number_particles):
        sampler = GaussianLRPosterior(model.prior, variation.d[n], variation.U[n], mean=particle.particles[n])
        noise = dl.Vector()
        model.prior.init_vector(noise, "noise")
        samples = []
        for m in range(N):
            s_prior = model.generate_vector(PARAMETER)
            s_posterior = model.generate_vector(PARAMETER)
            parRandom.normal(1., noise)
            model.prior.sample(noise, s_prior, add_mean=False)
            sampler.sample(s_prior, s_posterior, add_mean=True)
            samples.append(s_posterior)
            x_star = model.generate_vector()
            # update the parameter
            x_star[PARAMETER].zero()
            x_star[PARAMETER].axpy(1., s_posterior)
            # update the state at new parameter
            x_star[STATE].zero()
            model.solveFwd(x_star[STATE], x_star)

            # evaluate the cost functional, here the potential
            cost, reg, misfit = model.cost(x_star)
            print("posterior cost, reg, misfit = ", cost, reg, misfit)

            s_prior = model.generate_vector(PARAMETER)
            model.prior.sample(noise, s_prior, add_mean=True)
            x_star[PARAMETER].zero()
            x_star[PARAMETER].axpy(1., s_prior)
            # update the state at new parameter
            x_star[STATE].zero()
            model.solveFwd(x_star[STATE], x_star)

            # evaluate the cost functional, here the potential
            cost, reg, misfit = model.cost(x_star)
            print("prior cost, reg, misfit = ", cost, reg, misfit)


        mean = model.generate_vector(PARAMETER)
        mhelp = model.generate_vector(PARAMETER)

        for m in range(N):
            mean.axpy(1./N, samples[m])
        model.prior.M.mult(mean, mhelp)
        print("mean", np.sqrt(mean.inner(mhelp)))

        variance = model.generate_vector(PARAMETER)
        vhelp = model.generate_vector(PARAMETER)
        for m in range(N):
            vhelp = model.generate_vector(PARAMETER)
            vhelp.axpy(1.0, samples[m])
            vhelp.axpy(-1.0, mean)
            vhelp.set_local(vhelp.get_local() ** 2)
            variance.axpy(1.0 / N, vhelp)

        vhelp = model.generate_vector(PARAMETER)
        model.prior.M.mult(variance, vhelp)
        print("variance", np.sqrt(variance.inner(vhelp)))
