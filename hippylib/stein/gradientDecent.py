# contact: Peng Chen, peng@ices.utexas.edu, written on Novemeber 19, 2018

from __future__ import absolute_import, division, print_function

import dolfin as dl
import numpy as np
from ..modeling.variables import STATE, PARAMETER


class GradientDecent:
    # solve the optimization problem by Newton method with separated linear system
    def __init__(self, model, particle, variation, kernel, options):
        self.model = model  # forward model
        self.particle = particle  # set of particles, pn = particles[m]
        self.variation = variation
        self.kernel = kernel
        self.options = options
        self.save_kernel = options["save_kernel"]
        self.add_number = options["add_number"]
        self.add_step = options["add_step"]
        self.save_step = options["save_step"]
        self.save_number = options["save_number"]
        self.search_size = options["search_size"]

        self.it = 0
        self.converged = False
        self.reason = 0
        self.rank = dl.MPI.rank(self.model.prior.R.mpi_comm())

        self.gradient_norm = np.zeros(self.particle.number_particles)
        self.gradient_norm_init = np.zeros(self.particle.number_particles)
        self.pg_phat = np.zeros(self.particle.number_particles)
        self.tol_gradient = np.zeros(self.particle.number_particles)
        self.final_grad_norm = np.zeros(self.particle.number_particles)
        self.cost_new = np.zeros(self.particle.number_particles_all)
        self.reg_new = np.zeros(self.particle.number_particles_all)
        self.misfit_new = np.zeros(self.particle.number_particles_all)
        self.alpha = 1.e-2 * np.ones(self.particle.number_particles_all)
        self.n_backtrack = np.zeros(self.particle.number_particles_all)

    def gradientSeparated(self, gradient, m):

        if self.save_kernel:
            kernel_value = self.kernel.value_set[m]
            kernel_gradient = self.kernel.gradient_set[m]
        else:
            kernel_value = self.kernel.value(self.variation, m)
            kernel_gradient = self.kernel.gradient(self.variation, m)

        if self.kernel.delta_kernel:
            gp_misfit = self.model.generate_vector(PARAMETER)
            self.variation.gradient(gp_misfit, m)
            gradient.axpy(kernel_value[m], gp_misfit)
            gradient.axpy(-1.0, kernel_gradient[m])
        else:
            for ie in range(self.particle.number_particles):  # take the expectation over particle set
                gp_misfit = self.model.generate_vector(PARAMETER)
                self.variation.gradient(gp_misfit, ie)
                gradient.axpy(kernel_value[ie], gp_misfit)
                gradient.axpy(-1.0, kernel_gradient[ie])

            # also use the particle to compute the expectation
            if m >= self.particle.number_particles:
                gp_misfit = self.model.generate_vector(PARAMETER)
                self.variation.gradient(gp_misfit, m)
                gradient.axpy(kernel_value[m], gp_misfit)
                gradient.axpy(-1.0, kernel_gradient[m])

        tmp = self.model.generate_vector(PARAMETER)
        self.model.prior.Msolver.solve(tmp, gradient)
        gradient_norm = np.sqrt(gradient.inner(tmp))

        return gradient_norm

    def solve(self):
        # use gradient decent method to solve the optimization problem
        rel_tol = self.options["rel_tolerance"]
        abs_tol = self.options["abs_tolerance"]
        max_iter = self.options["max_iter"]
        inner_tol = self.options["inner_rel_tolerance"]
        print_level = self.options["print_level"]
        line_search = self.options["line_search"]
        c_armijo = self.options["c_armijo"]
        max_backtracking_iter = self.options["max_backtracking_iter"]

        self.variation.gauss_newton_approx = (self.it < self.variation.max_iter_gauss_newton_approx) \
                                             or self.variation.gauss_newton_approx_hold
        self.variation.update(self.particle)
        self.kernel.delta_kernel = (self.it < self.kernel.max_iter_delta_kernel) or self.kernel.delta_kernel_hold
        self.kernel.update(self.particle, self.variation)

        self.it = 0
        self.converged = False

        if self.save_number:
            self.kernel.save_values(self.save_number, self.it)
            self.particle.save(self.save_number, self.it)
            self.variation.save_eigenvalue(self.save_number, self.it)
            self.variation.plot_eigenvalue(self.save_number, self.it)

        while self.it < max_iter and (self.converged is False):
            # always update variation and kernel before compute the gradient
            self.variation.update(self.particle)
            self.kernel.update(self.particle, self.variation)

            phat = [self.model.generate_vector(PARAMETER) for m in range(self.particle.number_particles)]
            self.pg_phat = np.ones(self.particle.number_particles)
            for m in range(self.particle.number_particles):  # solve for each particle
                # evaluate gradient
                gradient = self.model.generate_vector(PARAMETER)
                self.gradient_norm[m] = self.gradientSeparated(gradient, m)

                phat[m].axpy(-1.0, gradient)
                self.pg_phat[m] = gradient.inner(phat[m])

                # set tolerance for gradient iteration
                if self.it == 0:
                    self.gradient_norm_init[m] = self.gradient_norm[m]
                    self.tol_gradient[m] = max(abs_tol, self.gradient_norm_init[m] * rel_tol)

                if self.particle.number_particles > self.particle.number_particles_old:
                        self.gradient_norm_init[m] = self.gradient_norm[m]
                        self.tol_gradient[m] = max(abs_tol, self.gradient_norm_init[m] * rel_tol)

            # step for particle update, pstep(x) = sum_n phat_n k_n(x)
            pstep = [self.model.generate_vector(PARAMETER) for m in range(self.particle.number_particles_all)]
            for m in range(self.particle.number_particles_all):
                for n in range(self.particle.number_particles):
                    pstep[m].axpy(self.kernel.value_set[n][m], phat[n])
                if m >= self.particle.number_particles:
                    pstep[m].axpy(self.kernel.value_set[m][m], phat[m])

            self.alpha = self.search_size * np.ones(self.particle.number_particles_all)
            self.n_backtrack = np.zeros(self.particle.number_particles_all)
            self.cost_new = np.zeros(self.particle.number_particles_all)
            self.reg_new = np.zeros(self.particle.number_particles_all)
            self.misfit_new = np.zeros(self.particle.number_particles_all)

            for m in range(self.particle.number_particles_all):
                # compute the old cost
                x = self.variation.x_all[m]
                cost_old, reg_old, misfit_old = self.model.cost(x)
                self.cost_new[m], self.reg_new[m], self.misfit_new[m] = cost_old, reg_old, misfit_old

                if line_search:
                    # do line search
                    descent = 0
                    x_star = self.model.generate_vector()
                    while descent == 0 and self.n_backtrack[m] < max_backtracking_iter:
                        # update the parameter
                        x_star[PARAMETER].zero()
                        x_star[PARAMETER].axpy(1., x[PARAMETER])
                        x_star[PARAMETER].axpy(self.alpha[m], pstep[m])
                        # update the state at new parameter
                        x_star[STATE].zero()
                        x_star[STATE].axpy(1., x[STATE])
                        self.model.solveFwd(x_star[STATE], x_star, inner_tol)

                        # evaluate the cost functional, here the potential
                        self.cost_new[m], self.reg_new[m], self.misfit_new[m] = self.model.cost(x_star)

                        # Check if armijo conditions are satisfied
                        if m < self.particle.number_particles:
                            if (self.cost_new[m] < cost_old + self.alpha[m] * c_armijo * self.pg_phat[m]) or \
                                    (-self.pg_phat[m] <= self.options["gdm_tolerance"]):
                                cost_old = self.cost_new[m]
                                descent = 1
                            else:
                                self.n_backtrack[m] += 1
                                self.alpha[m] *= 0.5
                                # print("alpha = ", alpha[m])
                        else:  # we do not have pg_phat for m >= particle.number_particles
                            if self.cost_new[m] < cost_old:
                                cost_old = self.cost_new[m]
                                descent = 1
                            else:
                                self.n_backtrack[m] += 1
                                self.alpha[m] *= 0.5

            # move all particles in the new directions, pm = pm + alpha[m] * sum_n phat[n] * k(pn, pm)
            for m in range(self.particle.number_particles_all):
                self.particle.particles[m].axpy(self.alpha[m], pstep[m])

            # print data
            if (self.rank == 0) and (print_level >= -1):
                print("\n{0:5} {1:8} {2:15} {3:15} {4:15} {5:14} {6:14}".format(
                    "It", "id", "cost", "misfit", "reg", "||g||L2", "alpha"))
                for m in range(self.particle.number_particles):
                    print("{0:3d} {1:3d} {2:15e} {3:15e} {4:15e} {5:15e} {6:14e}".format(
                    self.it, m, self.cost_new[m], self.misfit_new[m], self.reg_new[m], self.gradient_norm[m], self.alpha[m]))
                for m in range(self.particle.number_particles, self.particle.number_particles_all):
                    print("{0:3d} {1:3d} {2:15e} {3:15e} {4:15e} {5:15e} {6:14e}".format(
                        self.it, m, self.cost_new[m], self.misfit_new[m], self.reg_new[m], 0., self.alpha[m]))

            # verify stopping criteria
            done = True
            for m in range(self.particle.number_particles):
                self.final_grad_norm[m] = self.gradient_norm[m]
                if self.gradient_norm[m] > self.tol_gradient[m]:
                    done = False
            if done:
                self.converged = True
                self.reason = 1
                print("Termination reason: ", self.options["termination_reasons"][self.reason])
                if self.save_number:
                    self.kernel.save_values(self.save_number, self.it)
                    self.particle.save(self.save_number, self.it)
                    self.variation.save_eigenvalue(self.save_number, self.it)
                    self.variation.plot_eigenvalue(self.save_number, self.it)
                break

            done = True
            for m in range(self.particle.number_particles_all):  # should use _all
                if self.n_backtrack[m] < max_backtracking_iter:
                    done = False
            if done:
                self.converged = False
                self.reason = 2
                print("Termination reason: ", self.options["termination_reasons"][self.reason])
                if self.save_number:
                    self.kernel.save_values(self.save_number, self.it)
                    self.particle.save(self.save_number, self.it)
                    self.variation.save_eigenvalue(self.save_number, self.it)
                    self.variation.plot_eigenvalue(self.save_number, self.it)
                break

            done = True
            for m in range(self.particle.number_particles):
                self.final_grad_norm[m] = self.gradient_norm[m]
                if -self.pg_phat[m] > self.options["gdm_tolerance"]:
                    done = False
            if done:
                self.converged = True
                self.reason = 3
                print("Termination reason: ", self.options["termination_reasons"][self.reason])
                if self.save_number:
                    self.kernel.save_values(self.save_number, self.it)
                    self.particle.save(self.save_number, self.it)
                    self.variation.save_eigenvalue(self.save_number, self.it)
                    self.variation.plot_eigenvalue(self.save_number, self.it)
                break

            # update data for optimization in next step
            self.it += 1

            if self.it == max_iter:
                self.converged = False
                self.reason = 0
                print("Termination reason: ", self.options["termination_reasons"][self.reason])
                if self.save_number:
                    self.kernel.save_values(self.save_number, self.it)
                    self.particle.save(self.save_number, self.it)
                    self.variation.save_eigenvalue(self.save_number, self.it)
                    self.variation.plot_eigenvalue(self.save_number, self.it)
                break

            # add new particles if needed, try different adding criteria, e.g., np.max(self.tol_cg) < beta^{-t}
            if self.add_number and np.mod(self.it, self.add_step) == 0:
                self.particle.add(self.variation)

            # update variation, kernel, and hessian with new particles before solving the Newton linear system
            self.variation.gauss_newton_approx = (self.it < self.variation.max_iter_gauss_newton_approx) \
                                                 or self.variation.gauss_newton_approx_hold
            self.variation.update(self.particle)
            self.kernel.delta_kernel = (self.it < self.kernel.max_iter_delta_kernel) or self.kernel.delta_kernel_hold
            self.kernel.update(self.particle, self.variation)

            # expand the arrays if new particles are added
            if self.particle.number_particles > self.particle.number_particles_old:
                for m in range(self.particle.number_particles_old, self.particle.number_particles):
                    self.gradient_norm = np.append(self.gradient_norm, 0.)
                    self.gradient_norm_init = np.append(self.gradient_norm_init, 0.)
                    self.tol_gradient = np.append(self.tol_gradient, 0)
                    self.final_grad_norm = np.append(self.final_grad_norm, 0.)

            # save the particles for visualization and plot the eigenvalues at each particle
            if self.save_number and np.mod(self.it, self.save_step) == 0:
                self.kernel.save_values(self.save_number, self.it)
                self.particle.save(self.save_number, self.it)
                self.variation.save_eigenvalue(self.save_number, self.it)
                self.variation.plot_eigenvalue(self.save_number, self.it)

    def solve_adam(self):
        # D.P. Kingma, J. Ba. Adam: A method for stochastic optimization. https://arxiv.org/pdf/1412.6980.pdf
        # to be tuned, it does not seem to work well
        rel_tol = self.options["rel_tolerance"]
        abs_tol = self.options["abs_tolerance"]
        max_iter = self.options["max_iter"]
        inner_tol = self.options["inner_rel_tolerance"]
        print_level = self.options["print_level"]

        max_backtracking_iter = self.options["max_backtracking_iter"]

        gradient_norm = np.zeros(self.particle.number_particles)
        gradient_norm_init = np.zeros(self.particle.number_particles)
        tol_gradient = np.zeros(self.particle.number_particles)
        cost_new = np.zeros(self.particle.number_particles)
        reg_new = np.zeros(self.particle.number_particles)
        misfit_new = np.zeros(self.particle.number_particles)

        self.it = 0
        alpha = 1.e-3 * np.ones(self.particle.number_particles)
        beta_1 = 0.9
        beta_2 = 0.999
        mt_1 = [self.model.generate_vector(PARAMETER) for m in range(self.particle.number_particles)]
        mt_2 = [self.model.generate_vector(PARAMETER) for m in range(self.particle.number_particles)]

        vt_1 = [self.model.generate_vector(PARAMETER) for m in range(self.particle.number_particles)]
        vt_2 = [self.model.generate_vector(PARAMETER) for m in range(self.particle.number_particles)]

        epsilon = 1.e-8

        while self.it < max_iter and (self.converged is False):
            # always update variation and kernel before compute the gradient
            self.variation.update(self.particle)
            self.kernel.update(self.particle, self.variation)

            gt = [self.model.generate_vector(PARAMETER) for m in range(self.particle.number_particles)]
            n_backtrack = np.zeros(self.particle.number_particles)
            for m in range(self.particle.number_particles):  # solve for each particle
                # evaluate gradient
                gradient = self.model.generate_vector(PARAMETER)
                gradient_norm[m] = self.gradientSeparated(gradient, m)

                gt[m].axpy(1.0, gradient)

                if self.it == 0:
                    gradient_norm_init[m] = gradient_norm[m]
                    tol_gradient[m] = max(abs_tol, gradient_norm_init[m] * rel_tol)

                mt_2[m].axpy(beta_1, mt_1[m])
                mt_2[m].axpy((1-beta_1), gt[m])
                mt_1[m].zero()
                mt_1[m].axpy(1./(1-np.power(beta_1, self.it+1)), mt_2[m])

                vt_2[m].axpy(beta_2, vt_1[m])
                gt_array = gt[m].get_local()
                gt[m].set_local(gt_array**2)
                vt_2[m].axpy((1-beta_2), gt[m])
                vt_1[m].axpy(1./(1-np.power(beta_2, self.it+1)), vt_2[m])

                mt_array = mt_1[m].get_local()
                vt_array = vt_1[m].get_local()
                gt[m].set_local(mt_array/(np.sqrt(vt_array) + epsilon))

                # update particles
                self.particle.particles[m].axpy(-alpha[m], gt[m])

                # update moments
                mt_1[m].zero()
                mt_1[m].axpy(1.0, mt_2[m])
                vt_1[m].zero()
                vt_1[m].axpy(1.0, vt_2[m])

                # compute cost
                x_star = self.model.generate_vector()
                x_star[PARAMETER].zero()
                x_star[PARAMETER].axpy(1., self.particle.particles[m])
                x_star[STATE].zero()
                self.model.solveFwd(x_star[STATE], x_star, inner_tol)

                cost_new[m], reg_new[m], misfit_new[m] = self.model.cost(x_star)

            if (self.rank == 0) and (print_level >= -1):
                print("\n{0:5} {1:8} {2:15} {3:15} {4:15} {5:14} {6:14}".format(
                    "It", "id", "cost", "misfit", "reg", "||g||L2", "alpha"))
                for m in range(self.particle.number_particles):
                    print("{0:3d} {1:3d} {2:15e} {3:15e} {4:15e} {5:15e} {6:14e}".format(
                        self.it, m, cost_new[m], misfit_new[m], reg_new[m], gradient_norm[m], alpha[m]))

            done = True
            for m in range(self.particle.number_particles):
                self.final_grad_norm[m] = gradient_norm[m]
                if gradient_norm[m] > tol_gradient[m]:
                    done = False
            if done:
                self.converged = True
                self.reason = 1
                break

            done = True
            for m in range(self.particle.number_particles):
                if n_backtrack[m] < max_backtracking_iter:
                    done = False
            if done:
                self.converged = False
                self.reason = 2
                break

            self.it += 1
