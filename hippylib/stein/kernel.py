# contact: Peng Chen, peng@ices.utexas.edu, written on Novemeber 19, 2018

from __future__ import absolute_import, division, print_function

import numpy as np
from ..modeling.variables import PARAMETER
from .metric import MetricPrior, MetricPosteriorSeparate, MetricPosteriorAverage

import os
if not os.path.isdir("data"):
    os.mkdir("data")
import pickle

class Kernel:
    # evaluate the value and the gradient of the kernel at given particles
    def __init__(self, model, particle, variation, options):

        self.model = model  # forward model
        self.particle = particle  # particle class
        self.variation = variation
        self.options = options
        self.save_kernel = options["save_kernel"]
        self.delta_kernel = options["delta_kernel"]
        self.delta_kernel_hold = options["delta_kernel"]
        self.max_iter_delta_kernel = options["max_iter_delta_kernel"]
        self.type_Hessian = options["type_Hessian"]
        self.type_metric = options["type_metric"]
        self.type_scaling = options["type_scaling"]

        # metric M in k(pn,pm) = exp(-(pn-pm)^T * M * (pn-pm))
        if self.type_metric is 'prior':
            self.metric = MetricPrior(model, particle, variation, options)
        elif self.type_metric is 'posterior_average':
            self.metric = MetricPosteriorAverage(model, particle, variation, options)
        elif self.type_metric is 'posterior_separate':
            self.metric = MetricPosteriorSeparate(model, particle, variation, options)
        else:
            raise NotImplementedError("required metric is not implemented")

        self.value_set = []
        for n in range(particle.number_particles_all):
            values = np.zeros(particle.number_particles_all)
            self.value_set.append(values)

        if self.save_kernel:
            self.gradient_set = []
            for n in range(particle.number_particles_all):
                gradients = [self.model.generate_vector(PARAMETER) for ie in range(particle.number_particles)]
                self.gradient_set.append(gradients)

        self.value_sum = np.zeros(particle.number_particles)
        self.gradient_sum = [self.model.generate_vector(PARAMETER) for ie in range(particle.number_particles)]

        self.phelp = model.generate_vector(PARAMETER)

    def update(self, particle, variation):

        self.particle = particle
        self.variation = variation

        # update the size of the data if new particles are added
        if particle.number_particles_all > particle.number_particles_all_old:
            self.value_set = []
            for n in range(particle.number_particles_all):
                values = np.zeros(particle.number_particles_all)
                self.value_set.append(values)

            if self.save_kernel:
                self.gradient_set = []
                for n in range(particle.number_particles_all):
                    gradients = [self.model.generate_vector(PARAMETER) for m in range(particle.number_particles_all)]
                    self.gradient_set.append(gradients)

        # evaluate M * (pn-pm) and (pn-pm)^T * M * (pn-pm)
        scaling = np.zeros(particle.number_particles_all)
        for n in range(particle.number_particles_all):
            for m in range(particle.number_particles_all):
                pn = self.particle.particles[n]
                pm = self.particle.particles[m]
                self.phelp.zero()
                self.phelp.axpy(1.0, pn)
                self.phelp.axpy(-1.0, pm)
                pout = self.model.generate_vector(PARAMETER)
                self.metric.mult(self.phelp, pout, n, rescale=False)
                self.value_set[n][m] = self.phelp.inner(pout)  # (pn-pm)^T * M * (pn-pm)
                if self.save_kernel:
                    self.gradient_set[n][m].zero()
                    if m != n and not self.delta_kernel:
                        self.gradient_set[n][m].axpy(1., pout)  # M * (pn-pm)
            scaling[n] = np.mean(self.value_set[n])  #/ np.maximum(1, np.log(particle.number_particles))
            if scaling[n] == 0:  # in case of only one particle
                scaling[n] = 1

        self.metric.update(particle, variation, scaling)

        for n in range(particle.number_particles_all):
            if self.type_scaling == 1:
                # rescaling by the parameter dimension = trace(I)
                scaling[n] = self.model.problem.Vh[PARAMETER].dim()
            elif self.type_scaling == 2:
                # rescaling by the trace = trace(I) + trace(H)
                scaling[n] = (self.model.problem.Vh[PARAMETER].dim() + np.sum(self.variation.d[n]))
            elif self.type_scaling == 3:
                pass

            scale = scaling[n]
            if self.type_metric is "posterior_average":
                scale = np.median(scaling)

            for m in range(particle.number_particles_all):
                # evaluate the kernel k(pn,pm) = exp(-(pn-pm)^T * M * (pn-pm))
                if m != n and self.delta_kernel:
                    self.value_set[n][m] = 0.
                else:
                    self.value_set[n][m] = np.exp(-self.value_set[n][m]/scale)
                if self.save_kernel:
                    # evaluate the gradient of kernel at (pn, pm) with respect to pm,
                    # grad k(pn, pm) = 2 * k(pn, pm) * M * (pn - pm), where M is rescaled by dividing scale
                    self.gradient_set[n][m][:] *= 2.*self.value_set[n][m]/scale
            print("kernel = ", self.value_set[n], "scale = ", scale)

        # save the lumped kernel and gradient
        if self.save_kernel:
            self.value_sum = np.zeros(particle.number_particles_all)
            self.gradient_sum = [self.model.generate_vector(PARAMETER) for ie in range(particle.number_particles_all)]
            for n in range(particle.number_particles):
                for ie in range(particle.number_particles_all):
                    self.value_sum[ie] = self.value_sum[ie] + self.value_set[n][ie]
                    self.gradient_sum[ie].axpy(1.0, self.gradient_set[n][ie])

    def value(self, n=0, m=0):
        # evaluate the kernel k(pn,pm) = exp(-(pn-pm)^T * M * (pn-pm))
        pn = self.particle.particles[n]  # n = 1, ..., particle.number_particles
        pm = self.particle.particles[m]  # m = 1, ..., particle.number_particles_all
        self.phelp.zero()
        self.phelp.axpy(1.0, pn)
        self.phelp.axpy(-1.0, pm)

        value = np.exp(-self.metric.inner(self.phelp, self.phelp, n=n))  # k(pn,pm) = exp(-(pn-pm)^T * M * (pn-pm))

        return value

    def values(self, n=0):
        # evaluate the kernel at (pn, pm) for given n and all m = 1, ..., N
        values = np.zeros(self.particle.number_particles_all)
        for m in range(self.particle.number_particles_all):
            if m != n and self.delta_kernel:  # set zero of small kernels
                values[m] = 0
            else:
                values[m] = self.value(n=n, m=m)
        print("kernel values = ", values)
        self.value_set[n] = values

        return values

    def gradient(self, n=0, m=0):
        # evaluate the gradient of kernel at (pn, pm) with respect to pm, grad k(pn, pm) = 2 * k(pn, pm) * M * (pn - pm)
        pn = self.particle.particles[n]
        pm = self.particle.particles[m]
        self.phelp.zero()
        self.phelp.axpy(1.0, pn)
        self.phelp.axpy(-1.0, pm)

        ghelp = self.model.generate_vector(PARAMETER)
        self.metric.mult(self.phelp, ghelp, n=n)
        value = self.value(n=n, m=m)
        gradient = self.model.generate_vector(PARAMETER)
        gradient.axpy(2.*value, ghelp)

        return gradient

    def gradients(self, n=0):
        # evaluate the gradients of kernel at (pn, pm) with respect to pm, for all m = 1, ..., N
        gradients = [self.model.generate_vector(PARAMETER) for m in range(self.particle.number_particles_all)]
        for m in range(self.particle.number_particles_all):
            if m != n and self.delta_kernel:
                gradients[m].zero()
            else:
                gradients[m].axpy(1.0, self.gradient(n=n, m=m))

        if self.save_kernel:
            self.gradient_set[n] = gradients

        return gradients

    def save_values(self, save_number, it):
        for n in range(save_number):
            filename = 'data/kernel_' + str(n) + '_iteration_' + str(it) + '.p'
            pickle.dump(self.value_set, open(filename, 'wb'))

    def hessian(self):
        raise NotImplementedError(" the hessian needs to be implemented ")
