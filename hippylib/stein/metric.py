# contact: Peng Chen, peng@ices.utexas.edu, written on Novemeber 19, 2018

from __future__ import absolute_import, division, print_function

import numpy as np
from ..modeling.variables import PARAMETER


class MetricPrior:
    # define the metric M for the kernel k(pn, pm) = exp(-(pn-pm)^T * M * (pn - pm))
    def __init__(self, model, particle, variation, options):
        self.model = model  # forward model
        self.particle = particle
        self.variation = variation
        self.low_rank_Hessian_misfit = options["low_rank_Hessian_misfit"]
        self.type_scaling = options["type_scaling"]
        self.scaling = None

    def mult(self, phat, pout, n, rescale=True):

        pout.zero()
        # covariance/regularization
        phelp = self.model.generate_vector(PARAMETER)
        self.model.prior.R.mult(phat, phelp)
        pout.axpy(1.0, phelp)

        if rescale:
            if self.type_scaling == 1:
                # rescaling by dimension
                pout[:] *= 1. / self.model.problem.Vh[PARAMETER].dim()
            elif self.type_scaling == 2:
                # rescaling by the trace = trace(I) + trace(H), however the following is not the right trace
                 pout[:] *= 1./(self.model.problem.Vh[PARAMETER].dim() + np.sum(self.variation.d[n]))
            elif self.type_scaling == 3:
                # adaptive scaling by the distance
                pout[:] *= 1./self.scaling[n]

    def inner(self, phat1, phat2, n):

        phelp = self.model.generate_vector(PARAMETER)
        self.mult(phat1, phelp, n)

        return phat2.inner(phelp)

    def update(self, particle, variation, scaling):
        self.particle = particle
        self.variation = variation
        self.scaling = scaling


class MetricPosteriorSeparate:
    # define the metric M for the kernel k(pn, pm) = exp(-(pn-pm)^T * M * (pn - pm))
    def __init__(self, model, particle, variation, options):
        self.model = model  # forward model
        self.particle = particle
        self.variation = variation
        self.type_scaling = options["type_scaling"]
        self.scaling = None

    def mult(self, phat, pout, n, rescale=True):

        pout.zero()
        # covariance/regularization
        phelp = self.model.generate_vector(PARAMETER)
        self.model.prior.R.mult(phat, phelp)
        pout.axpy(1.0, phelp)
        # negative log likelihood function
        self.variation.hessian(phat, phelp, n)
        pout.axpy(1.0, phelp)

        if rescale:
            if self.type_scaling == 1:
                # rescaling by the parameter dimension = trace(I)
                pout[:] *= 1. / self.model.problem.Vh[PARAMETER].dim()
            elif self.type_scaling == 2:
                # rescaling by the trace = trace(I) + trace(H)
                 pout[:] *= 1./(self.model.problem.Vh[PARAMETER].dim() + np.sum(self.variation.d[n]))
            elif self.type_scaling == 3:
                # adaptive scaling by the distance
                pout[:] *= 1./self.scaling[n]

        # print("dim = ", self.model.problem.Vh[PARAMETER].dim(), "trace", np.sum(variation.d[n]))

    def inner(self, phat1, phat2, n):

        phelp = self.model.generate_vector(PARAMETER)
        self.mult(phat1, phelp, n)

        return phat2.inner(phelp)

    def update(self, particle, variation, scaling):
        self.particle = particle
        self.variation = variation
        self.scaling = scaling


class MetricPosteriorAverage:
    # define the metric M for the kernel k(pn, pm) = exp(-(pn-pm)^T * M * (pn - pm))
    def __init__(self, model, particle, variation, options):
        self.model = model  # forward model
        self.particle = particle
        self.variation = variation
        self.type_scaling = options["type_scaling"]
        self.scaling = None
        self.low_rank_Hessian_average = options["low_rank_Hessian_average"]
        self.low_rank_Hessian_misfit = options["low_rank_Hessian_misfit"]

    def mult(self, phat, pout, n, rescale=True):

        pout.zero()
        # covariance/regularization
        phelp = self.model.generate_vector(PARAMETER)
        self.model.prior.R.mult(phat, phelp)
        pout.axpy(1.0, phelp)
        # negative log likelihood function
        if self.low_rank_Hessian_average and self.low_rank_Hessian_misfit:
            self.variation.hessian_average(phat, pout)
        else:
            for n in range(self.particle.number_particles):
                self.variation.hessian(phat, phelp, n)
                pout.axpy(1.0 / self.particle.number_particles, phelp)

        if rescale:
            if self.type_scaling == 1:
                # rescaling by the parameter dimension = trace(I)
                pout[:] *= 1. / self.model.problem.Vh[PARAMETER].dim()
            elif self.type_scaling == 2:
                # rescaling by the trace = trace(I) + trace(H)
                 pout[:] *= 1./(self.model.problem.Vh[PARAMETER].dim() + np.sum(self.variation.d[n]))
            elif self.type_scaling == 3:
                # adaptive scaling by the distance
                pout[:] *= 1./np.median(self.scaling)

        # print("dim = ", self.model.problem.Vh[PARAMETER].dim(), "trace", np.sum(variation.d[n]))

    def inner(self, phat1, phat2, n):

        phelp = self.model.generate_vector(PARAMETER)
        self.mult(phat1, phelp, n)

        return phat2.inner(phelp)

    def update(self, particle, variation, scaling):
        self.particle = particle
        self.variation = variation
        self.scaling = scaling