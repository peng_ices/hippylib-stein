# contact: Peng Chen, peng@ices.utexas.edu, written on Novemeber 19, 2018

from __future__ import absolute_import, division, print_function

import dolfin as dl
import numpy as np
from ..modeling.variables import STATE, PARAMETER
from ..algorithms.cgsolverSteihaug import CGSolverSteihaug


# ########################################  separated Newton system ###################################################
class HessianSeparated:
    # construct a lumped Hessian action to solve Newton linear system
    def __init__(self, model, particle, variation, kernel, options):
        self.model = model
        self.particle = particle  # number of particles
        self.variation = variation
        self.kernel = kernel
        self.save_kernel = options["save_kernel"]
        self.type_Hessian = options["type_Hessian"]
        self.m = 0  # which particle
        self.ncalls = np.zeros(particle.number_particles_all, dtype=int)

    def init_vector(self, pvec, dim):
        self.model.prior.init_vector(pvec, dim)

    def update(self, particle, variation, kernel):
        # class assignment to be checked
        self.particle = particle
        self.variation = variation
        self.kernel = kernel

    def index(self, m):
        self.m = m
        self.variation.low_rank_Hessian_misfit = self.variation.low_rank_Hessian_misfit_hold
        self.variation.gauss_newton_approx = self.variation.gauss_newton_approx_hold

    def mult(self, phat, pout):

        pout.zero()

        if self.save_kernel:
            kernel_value = self.kernel.value_set[self.m]
            kernel_gradient = self.kernel.gradient_set[self.m]
        else:
            kernel_value = self.kernel.values(self.m)
            kernel_gradient = self.kernel.gradients(self.m)

        if self.kernel.delta_kernel:
            hessian_phat = self.model.generate_vector(PARAMETER)
            self.variation.hessian(phat, hessian_phat, self.m)

            R_coeff = 0.
            pout.axpy(kernel_value[self.m] ** 2, hessian_phat)
            pout.axpy(kernel_gradient[self.m].inner(phat), kernel_gradient[self.m])
            R_coeff += kernel_value[self.m] ** 2

            R_phat = self.model.generate_vector(PARAMETER)
            self.model.prior.R.mult(phat, R_phat)
            pout.axpy(R_coeff, R_phat)

        elif self.type_Hessian is "diagonal":
            R_coeff = 0.
            # loop over each particle to compute the expectation
            for ie in range(self.particle.number_particles):  # take the expectation
                hessian_phat = self.model.generate_vector(PARAMETER)
                self.variation.hessian(phat, hessian_phat, ie)
                pout.axpy(kernel_value[ie] ** 2, hessian_phat)
                pout.axpy(kernel_gradient[ie].inner(phat), kernel_gradient[ie])
                R_coeff += kernel_value[ie] ** 2

            # also use the particle to compute the expectation
            if self.m >= self.particle.number_particles:
                hessian_phat = self.model.generate_vector(PARAMETER)
                self.variation.hessian(phat, hessian_phat, self.m)
                pout.axpy(kernel_value[self.m] ** 2, hessian_phat)
                pout.axpy(kernel_gradient[self.m].inner(phat), kernel_gradient[self.m])
                R_coeff += kernel_value[self.m] ** 2

            R_phat = self.model.generate_vector(PARAMETER)
            self.model.prior.R.mult(phat, R_phat)
            pout.axpy(R_coeff, R_phat)

        elif self.type_Hessian is "lumped" and not self.kernel.delta_kernel:
            R_coeff = 0.
            # loop over each particle to compute the expectation
            for ie in range(self.particle.number_particles):  # take the expectation
                hessian_phat = self.model.generate_vector(PARAMETER)
                self.variation.hessian(phat, hessian_phat, ie)
                pout.axpy(self.kernel.value_sum[ie] * kernel_value[ie], hessian_phat)
                pout.axpy(kernel_gradient[ie].inner(phat), self.kernel.gradient_sum[ie])
                R_coeff += self.kernel.value_sum[ie] * kernel_value[ie]

                if self.m >= self.particle.number_particles:
                    pout.axpy(kernel_value[ie] ** 2, hessian_phat)
                    pout.axpy(kernel_gradient[ie].inner(phat), kernel_gradient[ie])
                    R_coeff += kernel_value[ie] ** 2

            # also use the particle to compute the expectation
            if self.m >= self.particle.number_particles:
                hessian_phat = self.model.generate_vector(PARAMETER)
                self.variation.hessian(phat, hessian_phat, self.m)
                pout.axpy(self.kernel.value_sum[self.m] * kernel_value[self.m], hessian_phat)
                pout.axpy(kernel_gradient[self.m].inner(phat), self.kernel.gradient_sum[self.m])
                R_coeff += self.kernel.value_sum[self.m] * kernel_value[self.m]
                pout.axpy(kernel_value[self.m] ** 2, hessian_phat)
                pout.axpy(kernel_gradient[self.m].inner(phat), kernel_gradient[self.m])
                R_coeff += kernel_value[self.m] ** 2

            R_phat = self.model.generate_vector(PARAMETER)
            self.model.prior.R.mult(phat, R_phat)
            pout.axpy(R_coeff, R_phat)

        else:
            raise NotImplementedError("choose between diagonal and lumped")

        self.ncalls[self.m] += 1


class NewtonSeparated:
    # solve the optimization problem by Newton method with separated linear system
    def __init__(self, model, particle, variation, kernel, options):
        self.model = model  # forward model
        self.particle = particle  # set of particles, pn = particles[m]
        self.variation = variation
        self.kernel = kernel
        self.options = options
        self.save_kernel = options["save_kernel"]
        self.add_number = options["add_number"]
        self.add_step = options["add_step"]
        self.save_step = options["save_step"]
        self.save_number = options["save_number"]

        self.it = 0
        self.converged = False
        self.reason = 0
        self.rank = dl.MPI.rank(self.model.prior.R.mpi_comm())
        self.gradient_norm = np.zeros(self.particle.number_particles_all)
        self.gradient_norm_init = np.zeros(self.particle.number_particles_all)
        self.pg_phat = np.zeros(self.particle.number_particles_all)
        self.tol_newton = np.zeros(self.particle.number_particles_all)
        self.tol_cg = np.zeros(self.particle.number_particles_all)
        self.total_cg_iter = np.zeros(self.particle.number_particles_all)
        self.final_grad_norm = np.zeros(self.particle.number_particles_all)
        self.cost_new = np.zeros(self.particle.number_particles_all)
        self.reg_new = np.zeros(self.particle.number_particles_all)
        self.misfit_new = np.zeros(self.particle.number_particles_all)
        self.alpha = np.ones(self.particle.number_particles_all)
        self.n_backtrack = np.zeros(self.particle.number_particles_all)

    def gradientSeparated(self, gradient, m):

        if self.save_kernel:
            kernel_value = self.kernel.value_set[m]
            kernel_gradient = self.kernel.gradient_set[m]
        else:
            kernel_value = self.kernel.values(self.variation, self.particle, m)
            kernel_gradient = self.kernel.gradients(self.variation, self.particle, m)

        if self.kernel.delta_kernel:
            gp_misfit = self.model.generate_vector(PARAMETER)
            self.variation.gradient(gp_misfit, m)
            gradient.axpy(kernel_value[m], gp_misfit)
            gradient.axpy(-1.0, kernel_gradient[m])
        else:
            for ie in range(self.particle.number_particles):  # take the expectation over particle set
                gp_misfit = self.model.generate_vector(PARAMETER)
                self.variation.gradient(gp_misfit, ie)
                gradient.axpy(kernel_value[ie], gp_misfit)
                gradient.axpy(-1.0, kernel_gradient[ie])

            # also use the particle to compute the expectation
            if m >= self.particle.number_particles:
                gp_misfit = self.model.generate_vector(PARAMETER)
                self.variation.gradient(gp_misfit, m)
                gradient.axpy(kernel_value[m], gp_misfit)
                gradient.axpy(-1.0, kernel_gradient[m])

        tmp = self.model.generate_vector(PARAMETER)
        self.model.prior.Msolver.solve(tmp, gradient)
        gradient_norm = np.sqrt(gradient.inner(tmp))

        return gradient_norm

    def solve(self):
        # use Newton method to solve the optimization problem
        rel_tol = self.options["rel_tolerance"]
        abs_tol = self.options["abs_tolerance"]
        max_iter = self.options["max_iter"]
        line_search = self.options["line_search"]
        inner_tol = self.options["inner_rel_tolerance"]
        print_level = self.options["print_level"]
        cg_coarse_tolerance = self.options["cg_coarse_tolerance"]
        c_armijo = self.options["c_armijo"]
        max_backtracking_iter = self.options["max_backtracking_iter"]

        hessian = HessianSeparated(self.model, self.particle, self.variation, self.kernel, self.options)

        self.variation.gauss_newton_approx = (self.it < self.variation.max_iter_gauss_newton_approx) \
                                             or self.variation.gauss_newton_approx_hold
        self.variation.update(self.particle)
        self.kernel.delta_kernel = (self.it < self.kernel.max_iter_delta_kernel) or self.kernel.delta_kernel_hold
        self.kernel.update(self.particle, self.variation)
        hessian.update(self.particle, self.variation, self.kernel)

        self.it = 0
        self.converged = False

        if self.save_number:
            self.kernel.save_values(self.save_number, self.it)
            self.particle.save(self.save_number, self.it)
            self.variation.save_eigenvalue(self.save_number, self.it)
            self.variation.plot_eigenvalue(self.save_number, self.it)

        while self.it < max_iter and (self.converged is False):

            if self.options["type_parameter"] is 'vector':
                self.particle.plot_particles(self.particle, self.it)

            phat = [self.model.generate_vector(PARAMETER) for m in range(self.particle.number_particles_all)]
            self.pg_phat = np.ones(self.particle.number_particles_all)
            for m in range(self.particle.number_particles_all):  # solve for each particle used for constructing the map
                # evaluate gradient
                gradient = self.model.generate_vector(PARAMETER)
                self.gradient_norm[m] = self.gradientSeparated(gradient, m)

                # set tolerance for Newton iteration
                if self.it == 0:
                    self.gradient_norm_init[m] = self.gradient_norm[m]
                    self.tol_newton[m] = max(abs_tol, self.gradient_norm_init[m] * rel_tol)

                if self.particle.number_particles_all > self.particle.number_particles_all_old:
                    if m >= self.particle.number_particles_old \
                            and m < self.particle.number_particles_all - self.particle.number_particles_add:
                        self.gradient_norm_init[m] = self.gradient_norm[m]
                        self.tol_newton[m] = max(abs_tol, self.gradient_norm_init[m] * rel_tol)

                # set tolerance for CG iteration
                self.tol_cg[m] = min(cg_coarse_tolerance, np.sqrt(self.gradient_norm[m] / self.gradient_norm_init[m]))

                # set Hessian and CG solver
                hessian.index(m)  # which particle to deal with
                solver = CGSolverSteihaug(comm=self.model.prior.R.mpi_comm())
                solver.set_operator(hessian)
                solver.set_preconditioner(self.model.Rsolver())
                solver.parameters["rel_tolerance"] = self.tol_cg[m]
                solver.parameters["zero_initial_guess"] = True
                solver.parameters["print_level"] = print_level
                solver.parameters["max_iter"] = 20

                # solve the Newton linear system
                solver.solve(phat[m], -gradient)

                # replace Hessian with its Gauss Newton approximation if CG reached a negative direction
                if solver.reasonid == 2:
                    hessian.variation.low_rank_Hessian_misfit = False
                    hessian.variation.gauss_newton_approx = True
                    solver.set_operator(hessian)
                    solver.solve(phat[m], -gradient)

                self.total_cg_iter[m] += hessian.ncalls[m]
                self.pg_phat[m] = gradient.inner(phat[m])

            # step for particle update, pstep(x) = sum_n phat_n k_n(x)
            pstep = [self.model.generate_vector(PARAMETER) for m in range(self.particle.number_particles_all)]
            for m in range(self.particle.number_particles_all):
                for n in range(self.particle.number_particles):
                    pstep[m].axpy(self.kernel.value_set[n][m], phat[n])
                if m >= self.particle.number_particles:
                    pstep[m].axpy(self.kernel.value_set[m][m], phat[m])

            self.alpha = np.ones(self.particle.number_particles_all)
            self.n_backtrack = np.zeros(self.particle.number_particles_all)
            self.cost_new = np.zeros(self.particle.number_particles_all)
            self.reg_new = np.zeros(self.particle.number_particles_all)
            self.misfit_new = np.zeros(self.particle.number_particles_all)

            for m in range(self.particle.number_particles_all):
                # compute the old cost
                x = self.variation.x_all[m]
                cost_old, reg_old, misfit_old = self.model.cost(x)
                self.cost_new[m], self.reg_new[m], self.misfit_new[m] = cost_old, reg_old, misfit_old

                if line_search:
                    # do line search
                    descent = 0
                    x_star = self.model.generate_vector()
                    while descent == 0 and self.n_backtrack[m] < max_backtracking_iter:
                        # update the parameter
                        x_star[PARAMETER].zero()
                        x_star[PARAMETER].axpy(1., x[PARAMETER])
                        x_star[PARAMETER].axpy(self.alpha[m], pstep[m])
                        # update the state at new parameter
                        x_star[STATE].zero()
                        x_star[STATE].axpy(1., x[STATE])
                        self.model.solveFwd(x_star[STATE], x_star, inner_tol)

                        # evaluate the cost functional, here the potential
                        self.cost_new[m], self.reg_new[m], self.misfit_new[m] = self.model.cost(x_star)

                        # Check if armijo conditions are satisfied
                        if m < self.particle.number_particles:
                            if (self.cost_new[m] < cost_old + self.alpha[m] * c_armijo * self.pg_phat[m]) or \
                                    (-self.pg_phat[m] <= self.options["gdm_tolerance"]):
                                cost_old = self.cost_new[m]
                                descent = 1
                            else:
                                self.n_backtrack[m] += 1
                                self.alpha[m] *= 0.5
                                # print("self.alpha = ", self.alpha[m])
                        else:  # we do not have pg_phat for m >= particle.number_particles
                            if self.cost_new[m] < cost_old:
                                cost_old = self.cost_new[m]
                                descent = 1
                            else:
                                self.n_backtrack[m] += 1
                                self.alpha[m] *= 0.5

            mean, mean_norm = self.particle.mean()
            variance, variance_norm = self.particle.pointwise_variance()
            print("mean", mean_norm, "variance", variance_norm)

            # move all particles in the new directions, pm = pm + self.alpha[m] * sum_n phat[n] * k(pn, pm)
            for m in range(self.particle.number_particles_all):
                self.particle.particles[m].axpy(self.alpha[m], pstep[m])

            # print data
            if (self.rank == 0) and (print_level >= -1):
                print("\n{0:4} {1:4} {2:5} {3:15} {4:15} {5:15} {6:15} {7:14} {8:14} {9:14}".format(
                    "It", "id", "cg_it", "cost", "misfit", "reg", "(g,dm)", "||g||L2", "alpha", "tolcg"))
                for m in range(self.particle.number_particles_all):
                    print("{0:3d} {1:3d} {2:4d} {3:15e} {4:15e} {5:15e} {6:15e} {7:14e} {8:14e} {9:14e}".format(
                        self.it, m, hessian.ncalls[m], self.cost_new[m], self.misfit_new[m], self.reg_new[m],
                        self.pg_phat[m], self.gradient_norm[m], self.alpha[m], self.tol_cg[m]))
                # for m in range(self.particle.number_particles, self.particle.number_particles_all):
                #     print("{0:3d} {1:3d} {2:4d} {3:15e} {4:15e} {5:15e} {6:15e} {7:14e} {8:14e} {9:14e}".format(
                #         self.it, m, 0, self.cost_new[m], self.misfit_new[m], self.reg_new[m],
                #         0., 0., alpha[m], 0.))

            # verify stopping criteria
            done = True
            for m in range(self.particle.number_particles):
                self.final_grad_norm[m] = self.gradient_norm[m]
                if self.gradient_norm[m] > self.tol_newton[m]:
                    done = False
            if done:
                self.converged = True
                self.reason = 1
                print("Termination reason: ", self.options["termination_reasons"][self.reason])
                if self.save_number:
                    self.kernel.save_values(self.save_number, self.it)
                    self.particle.save(self.save_number, self.it)
                    self.variation.save_eigenvalue(self.save_number, self.it)
                    self.variation.plot_eigenvalue(self.save_number, self.it)
                break

            done = True
            for m in range(self.particle.number_particles_all):  # should use _all
                if self.n_backtrack[m] < max_backtracking_iter:
                    done = False
            if done:
                self.converged = False
                self.reason = 2
                print("Termination reason: ", self.options["termination_reasons"][self.reason])
                if self.save_number:
                    self.kernel.save_values(self.save_number, self.it)
                    self.particle.save(self.save_number, self.it)
                    self.variation.save_eigenvalue(self.save_number, self.it)
                    self.variation.plot_eigenvalue(self.save_number, self.it)
                break

            done = True
            for m in range(self.particle.number_particles):
                self.final_grad_norm[m] = self.gradient_norm[m]
                if -self.pg_phat[m] > self.options["gdm_tolerance"]:
                    done = False
            if done:
                self.converged = True
                self.reason = 3
                print("Termination reason: ", self.options["termination_reasons"][self.reason])
                if self.save_number:
                    self.kernel.save_values(self.save_number, self.it)
                    self.particle.save(self.save_number, self.it)
                    self.variation.save_eigenvalue(self.save_number, self.it)
                    self.variation.plot_eigenvalue(self.save_number, self.it)
                break

            # update data for optimization in next step
            self.it += 1

            if self.it == max_iter:
                self.converged = False
                self.reason = 0
                print("Termination reason: ", self.options["termination_reasons"][self.reason])
                if self.save_number:
                    self.kernel.save_values(self.save_number, self.it)
                    self.particle.save(self.save_number, self.it)
                    self.variation.save_eigenvalue(self.save_number, self.it)
                    self.variation.plot_eigenvalue(self.save_number, self.it)
                break

            # add new particles if needed, try different adding criteria, e.g., np.max(self.tol_cg) < beta^{-t}
            if self.add_number and np.mod(self.it, self.add_step) == 0:
                self.particle.add(self.variation)
                # expand the arrays if new particles are added
                if self.particle.number_particles_all > self.particle.number_particles_all_old:
                    for m in range(self.particle.number_particles_all_old, self.particle.number_particles_all):
                        self.gradient_norm = np.insert(self.gradient_norm, m-self.particle.number_particles_add, 0.)
                        self.gradient_norm_init = np.insert(self.gradient_norm_init, m-self.particle.number_particles_add, 0.)
                        self.tol_newton = np.insert(self.tol_newton, m-self.particle.number_particles_add, 0)
                        self.tol_cg = np.insert(self.tol_cg, m-self.particle.number_particles_add, 0.)
                        self.total_cg_iter = np.insert(self.total_cg_iter, m-self.particle.number_particles_add, 0)
                        self.final_grad_norm = np.insert(self.final_grad_norm, m-self.particle.number_particles_add, 0.)
                        hessian.ncalls = np.insert(hessian.ncalls, m - self.particle.number_particles_add, 0)

            # update variation, kernel, and hessian with new particles before solving the Newton linear system
            self.variation.gauss_newton_approx = (self.it < self.variation.max_iter_gauss_newton_approx) \
                                                 or self.variation.gauss_newton_approx_hold
            self.variation.update(self.particle)
            self.kernel.delta_kernel = (self.it < self.kernel.max_iter_delta_kernel) or self.kernel.delta_kernel_hold
            self.kernel.update(self.particle, self.variation)
            hessian.update(self.particle, self.variation, self.kernel)

            # save the particles for visualization and plot the eigenvalues at each particle
            if self.save_number and np.mod(self.it, self.save_step) == 0:
                self.kernel.save_values(self.save_number, self.it)
                self.particle.save(self.save_number, self.it)
                self.variation.save_eigenvalue(self.save_number, self.it)
                self.variation.plot_eigenvalue(self.save_number, self.it)
