# contact: Peng Chen, peng@ices.utexas.edu, written on Novemeber 19, 2018

from __future__ import absolute_import, division, print_function

import numpy as np
import dolfin as dl
from ..modeling.variables import PARAMETER
from ..utils.random import parRandom
from ..modeling.posterior import GaussianLRPosterior
from ..utils.checkDolfinVersion import dlversion

plot_valid = True
try:
    import matplotlib.pyplot as plt
except:
    plot_valid = False
    print("can not import pyplot")

if plot_valid:
    import os
    if not os.path.isdir("figure"):
        os.mkdir("figure")

import os
if not os.path.isdir("data"):
    os.mkdir("data")


class Particle:
    # class to generate, move, and add particles
    def __init__(self, model, options):
        self.model = model
        self.options = options
        self.add_rule = options["add_rule"]
        self.add_number = options["add_number"]
        self.type_parameter = options["type_parameter"]
        self.particles = None

        self.number_particles = options["number_particles"]
        self.number_particles_old = self.number_particles
        self.number_particles_add = options["number_particles_add"]
        self.number_particles_all = self.number_particles + self.number_particles_add
        self.number_particles_all_old = self.number_particles_all

        particles = []
        noise = dl.Vector()
        self.model.prior.init_vector(noise, "noise")
        for n in range(self.number_particles):
            particle = self.model.generate_vector(PARAMETER)
            parRandom.normal(1., noise)
            self.model.prior.sample(noise, particle, add_mean=True)
            particles.append(particle)
        self.particles = particles

        if self.number_particles_add > 0:
            for n in range(self.number_particles_add):
                particle = self.model.generate_vector(PARAMETER)
                parRandom.normal(1., noise)
                self.model.prior.sample(noise, particle, add_mean=True)
                self.particles.append(particle)

    def add(self, variation):
        # add new particles by Laplace distribution at each of the particle for the transport map construction
        if self.add_rule:
            index = self.number_particles_all - self.number_particles_add
            for n in range(self.number_particles):
                sampler = GaussianLRPosterior(self.model.prior, variation.d[n], variation.U[n], mean=self.particles[n])
                noise = dl.Vector()
                self.model.prior.init_vector(noise, "noise")
                for m in range(self.add_number):
                    s_prior = self.model.generate_vector(PARAMETER)
                    s_posterior = self.model.generate_vector(PARAMETER)
                    parRandom.normal(1., noise)
                    self.model.prior.sample(noise, s_prior, add_mean=False)
                    sampler.sample(s_prior, s_posterior, add_mean=True)
                    self.particles.insert(index, s_posterior)
                    index += 1

            # update the number of particles used for constructing the transport map
            if self.add_rule == 1:  # add all the new particles for construction
                self.number_particles_old = self.number_particles
                self.number_particles += self.number_particles_old * self.add_number
            elif self.add_rule == 2:  # only add the ones added in the previous step
                self.number_particles_old = self.number_particles
                self.number_particles = self.number_particles_all - self.number_particles_add
            else:
                pass

            # update the total number of particles
            self.number_particles_all_old = self.number_particles_all
            self.number_particles_all += self.number_particles_old * self.add_number

    def mean(self):
        # compute the mean of the particles
        mean = self.model.generate_vector(PARAMETER)
        for m in range(self.number_particles_all):
            mean.axpy(1.0/self.number_particles_all, self.particles[m])

        mhelp = self.model.generate_vector(PARAMETER)
        self.model.prior.M.mult(mean, mhelp)

        return mean, np.sqrt(mean.inner(mhelp))

    def pointwise_variance(self):
        variance = self.model.generate_vector(PARAMETER)
        mean = self.model.generate_vector(PARAMETER)
        for m in range(self.number_particles_all):
            mean.axpy(1.0 / self.number_particles_all, self.particles[m])

        for m in range(self.number_particles_all):
            vhelp = self.model.generate_vector(PARAMETER)
            vhelp.axpy(1.0, self.particles[m])
            vhelp.axpy(-1.0, mean)
            vhelp.set_local(vhelp.get_local()**2)
            variance.axpy(1.0 / self.number_particles_all, vhelp)

        vhelp = self.model.generate_vector(PARAMETER)
        self.model.prior.M.mult(variance, vhelp)

        return variance, np.sqrt(variance.inner(vhelp))

    def save(self, save_number=1, it=0):
        # save samples for visualization
        if save_number > self.number_particles_all:
            save_number = self.number_particles_all

        for n in range(save_number):
            if self.type_parameter is 'field':
                particle_fun = dl.Function(self.model.problem.Vh[PARAMETER], name='particle')
                particle_fun.vector().axpy(1.0, self.particles[n])
                filename = 'data/particle_' + str(n) + '_iteration_' + str(it) + '.xdmf'
                if dlversion() <= (1, 6, 0):
                    dl.File(self.model.prior.R.mpi_comm(), filename) << particle_fun
                else:
                    xf = dl.XDMFFile(self.model.prior.R.mpi_comm(), filename)
                    xf.write(particle_fun)
            elif self.type_parameter is 'vector':
                filename = 'data/particle_' + str(n) + '_iteration_' + str(it)
                np.savez(filename, particle=self.particles[n].get_local())

    def plot_particles(self, particle, it):
        sample = np.zeros((2, particle.number_particles_all))
        for m in range(particle.number_particles_all):
            sample[:, m] = particle.particles[m].get_local()[:2]

        fig = plt.figure()
        plt.plot(sample[0, :], sample[1, :], 'r.')
        plt.axis('square')
        plt.xlim(-3., 3.)
        plt.ylim(-3., 3.)
        filename = "figure/samples_" + str(it) + '.pdf'
        fig.savefig(filename, format='pdf')
        plt.close()