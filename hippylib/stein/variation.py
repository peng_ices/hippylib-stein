# contact: Peng Chen, peng@ices.utexas.edu, written on Novemeber 19, 2018

from __future__ import absolute_import, division, print_function

import dolfin as dl
import numpy as np
from ..modeling.variables import STATE, PARAMETER, ADJOINT
from ..modeling.reducedHessian import ReducedHessian
from ..utils.random import Random
from ..algorithms.multivector import MultiVector
from ..algorithms.lowRankOperator import LowRankOperator
from ..algorithms.randomizedEigensolver import doublePass, doublePassG

import pickle

plot_valid = True
try:
    import matplotlib.pyplot as plt
except:
    plot_valid = False
    print("can not import pyplot")

if plot_valid:
    import os
    if not os.path.isdir("figure"):
        os.mkdir("figure")


class HessianAverage:
    # to compute one low rank decomposition of the metric
    def __init__(self, model, particle, d, U, low_rank_Hessian_misfit):
        self.model = model
        self.number_particles = particle.number_particles
        self.d = d
        self.U = U
        self.low_rank_Hessian_misfit = low_rank_Hessian_misfit

    def mult(self, phat, pout):
        if self.low_rank_Hessian_misfit == 1:  # pout = 1/N * \sum_n U_n Sigma_n U_n^T phat
            for n in range(self.number_particles):
                phelp = self.model.generate_vector(PARAMETER)
                hessian_misfit = LowRankOperator(self.d[n], self.U[n])
                hessian_misfit.mult(phat, phelp)
                pout.axpy(1.0 / self.number_particles, phelp)

        elif self.low_rank_Hessian_misfit == 2:
            phelp = self.model.generate_vector(PARAMETER)
            self.model.prior.R.mult(phat, phelp)
            phelp_out = self.model.generate_vector(PARAMETER)
            for n in range(self.number_particles):  # pout = 1/N * \sum_n R U_n Sigma_n U_n^T R phat
                phelp_1 = self.model.generate_vector(PARAMETER)
                hessian_misfit = LowRankOperator(self.d[n], self.U[n])
                hessian_misfit.mult(phelp, phelp_1)
                phelp_out.axpy(1.0 / self.number_particles, phelp_1)
            self.model.prior.R.mult(phelp_out, pout)

        else:
            raise NotImplementedError("please choose between 0, 1, 2 for low_rank_Hessian_misfit")


class Variation:
    # compute the variation (gradient, Hessian) of the negative log likelihood function
    def __init__(self, model, particle, options):
        # if add particles, then use generalized eigenvalue decomposition
        if options["add_number"] > 0:
            options["low_rank_Hessian_misfit"] = 2

        self.model = model  # forward model
        self.particle = particle  # particle class
        self.options = options
        self.low_rank_Hessian_misfit = options["low_rank_Hessian_misfit"]
        self.low_rank_Hessian_average = options["low_rank_Hessian_average"]
        self.low_rank_Hessian_misfit_hold = options["low_rank_Hessian_misfit"]
        self.rank_Hessian_misfit = options["rank_Hessian_misfit"]
        self.gauss_newton_approx = options["gauss_newton_approx"]
        self.gauss_newton_approx_hold = options["gauss_newton_approx"]
        self.max_iter_gauss_newton_approx = options["max_iter_gauss_newton_approx"]

        if options["add_number"] > 0:
            self.low_rank_Hessian_misfit = 2  # use generalized eigendecomposition if new particles are to be added
            self.low_rank_Hessian_misfit_hold = 2

        if self.low_rank_Hessian_misfit:
            # generate Gaussian random vectors to be used for randomized SVD
            randomGen = Random(myid=0, nproc=dl.MPI.size(model.problem.Vh[PARAMETER].mesh().mpi_comm()))
            m = model.generate_vector(PARAMETER)
            Omega = MultiVector(m, self.rank_Hessian_misfit + 5)
            for i in range(self.rank_Hessian_misfit + 5):
                randomGen.normal(1., Omega[i])
            self.Omega = Omega
            self.d_average = None
            self.U_average = None

        self.d = [None] * particle.number_particles_all
        self.U = [None] * particle.number_particles_all
        self.x_all = [None] * particle.number_particles_all
        for n in range(particle.number_particles_all):
            self.x_all[n] = [model.generate_vector(STATE), model.generate_vector(PARAMETER),
                             model.generate_vector(ADJOINT)]

    def update(self, particle):
        self.particle = particle

        if particle.number_particles_all > particle.number_particles_all_old:
            self.d = [None] * particle.number_particles_all
            self.U = [None] * particle.number_particles_all
            self.x_all = [None] * particle.number_particles_all
            for n in range(particle.number_particles_all):
                self.x_all[n] = [self.model.generate_vector(STATE), self.model.generate_vector(PARAMETER),
                                 self.model.generate_vector(ADJOINT)]

        # solve the forward and adjoint problems at all the particles
        for n in range(particle.number_particles_all):
            # solve the state and adjoint problems
            self.x_all[n][PARAMETER].zero()
            self.x_all[n][PARAMETER].axpy(1.0, particle.particles[n])
            self.model.solveFwd(self.x_all[n][STATE], self.x_all[n])
            self.model.solveAdj(self.x_all[n][ADJOINT], self.x_all[n])

        if self.low_rank_Hessian_misfit:
            # compute the low rank decomposition only for the particles used in constructing the transport map
            for n in range(particle.number_particles_all):
                # low rank decomposition of hessian misfit
                self.model.setPointForHessianEvaluations(self.x_all[n], self.gauss_newton_approx)
                hessian_misfit = ReducedHessian(self.model, misfit_only=True)
                if self.low_rank_Hessian_misfit == 1:
                    self.d[n], self.U[n] = doublePass(hessian_misfit, self.Omega, self.rank_Hessian_misfit, s=1)
                elif self.low_rank_Hessian_misfit == 2:
                    self.d[n], self.U[n] = doublePassG(hessian_misfit, self.model.prior.R, self.model.prior.Rsolver,
                                                       self.Omega, self.rank_Hessian_misfit, s=1)
                else:
                    raise NotImplementedError("please choose between 0, 1, 2 for low_rank_Hessian_misfit")

            if self.low_rank_Hessian_average:
                hessian_misfit = HessianAverage(self.model, particle, self.d, self.U, self.low_rank_Hessian_misfit)
                if self.low_rank_Hessian_misfit == 1:
                    self.d_average, self.U_average = doublePass(hessian_misfit, self.Omega, self.rank_Hessian_misfit, s=1)
                elif self.low_rank_Hessian_misfit == 2:
                    self.d_average, self.U_average = doublePassG(hessian_misfit, self.model.prior.R, self.model.prior.Rsolver,
                                                                 self.Omega, self.rank_Hessian_misfit, s=1)
                else:
                    raise NotImplementedError("please choose between 0, 1, 2 for low_rank_Hessian_misfit")

    def gradient(self, pout, n):
        self.model.evalGradientParameter(self.x_all[n], pout, misfit_only=False)

    def hessian(self, phat, pout, n):  # misfit Hessian action at particle pn in direction phat , H * phat = pout

        if self.low_rank_Hessian_misfit == 1:
            hessian_misfit = LowRankOperator(self.d[n], self.U[n])
            hessian_misfit.mult(phat, pout)
        elif self.low_rank_Hessian_misfit == 2:
            phelp = self.model.generate_vector(PARAMETER)
            self.model.prior.R.mult(phat, pout)
            hessian_misfit = LowRankOperator(self.d[n], self.U[n])
            hessian_misfit.mult(pout, phelp)
            self.model.prior.R.mult(phelp, pout)
        else:
            self.model.setPointForHessianEvaluations(self.x_all[n], self.gauss_newton_approx)
            hessian_misfit = ReducedHessian(self.model, misfit_only=True)
            hessian_misfit.mult(phat, pout)

    def hessian_average(self, phat, pout):

        if self.low_rank_Hessian_misfit == 1:
            hessian_misfit = LowRankOperator(self.d_average, self.U_average)
            hessian_misfit.mult(phat, pout)
        elif self.low_rank_Hessian_misfit == 2:
            phelp = self.model.generate_vector(PARAMETER)
            self.model.prior.R.mult(phat, pout)
            hessian_misfit = LowRankOperator(self.d_average, self.U_average)
            hessian_misfit.mult(pout, phelp)
            self.model.prior.R.mult(phelp, pout)

    def save_eigenvalue(self, save_number=1, it=0):
        for n in range(save_number):
            filename = "data/eigenvalue_" + str(n) + '_iteration_' + str(it) + '.p'
            pickle.dump(self.d, open(filename, 'wb'))

    def plot_eigenvalue(self, save_number=1, it=0):

        if plot_valid and self.low_rank_Hessian_misfit:

            for n in range(save_number):
                if n < self.particle.number_particles_all:
                    d = self.d[n]

                    fig = plt.figure()
                    if np.all(d > 0):
                        plt.semilogy(d, 'r.')
                    else:
                        indexplus = np.where(d > 0)[0]
                        # print indexplus
                        dplus, = plt.semilogy(indexplus, d[indexplus], 'ro')
                        indexminus = np.where(d < 0)[0]
                        # print indexminus
                        dminus, = plt.semilogy(indexminus, -d[indexminus], 'k*')

                        plt.legend([dplus, dminus], ["positive", "negative"])

                    plt.xlabel("n ", fontsize=12)
                    plt.ylabel("|$\lambda_n$|", fontsize=12)

                    plt.tick_params(axis='both', which='major', labelsize=12)
                    plt.tick_params(axis='both', which='minor', labelsize=12)

                    filename = "figure/eigenvalue_" + str(n) + '_iteration_' + str(it) + '.pdf'
                    fig.savefig(filename, format='pdf')
                    filename = "figure/eigenvalue_" + str(n) + '_iteration_' + str(it) + '.eps'
                    fig.savefig(filename, format='eps')

                    plt.close()
